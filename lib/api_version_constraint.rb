class ApiVersionConstraint
  def initialize(options)
    @version = options[:version]
    @default = options[:default]
  end

  #check the passed version.
  def matches?(req)
    @default || req.headers['Accept'].include?("application/vnd.loja-cd.v#{@version}")
  end
end
