require 'rails_helper'

RSpec.describe User, type: :model do
 let(:user) { build(:user) }

  it { is_expected.to validate_presence_of(:email) }
  it { is_expected.to validate_uniqueness_of(:email).case_insensitive }
  it { is_expected.to validate_confirmation_of(:password) }
  it { is_expected.to allow_value('rand@gmail.com').for(:email) }
  it { is_expected.to validate_uniqueness_of(:auth_token) }

  describe '#generate authentication_token!' do
    it 'generates a unique auth_token' do
      allow(Devise).to receive(:friendly_token).and_return('abc123xyzTOKEN')
      user.generate_authentication_token!
       
      expect(user.auth_token).to be_eql('abc123xyzTOKEN')
    end

    it 'generates a new token when token has already been taken' do
      
      allow(Devise).to receive(:friendly_token).and_return('abc123lalala', 'abc123lalala', 'abcxyz123345')
      existing_user = create(:user)
      user.generate_authentication_token!

      expect(user.auth_token).not_to be_eql(existing_user.auth_token)
    end
  end

end
